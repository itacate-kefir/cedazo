# Indexado

Cada sitio Jekyll crea su propio índice, esto permite:

* Que el índice se distribuya junto con los archivos, de forma que se
  pueda usar Xapian localmente

* Que el indexado se realice con un plugin de Jekyll

* Que no haya que escribir una API específica para indexar, teniendo que
  meterse con autenticación.

# Búsqueda

El buscador es una API web que abre las bases de datos de cada sitio y
devuelve resultados en formato JSON.

* No necesita autenticación, porque las búsquedas son públicas

* Devuelve los datos en JSONAPI, para que el sitio que hace la búsqueda
  (un JavaScript) realice algo con los valores

* Es de solo lectura
