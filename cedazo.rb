# Copyright (C) 2018 f <f@kefir.red>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.
require 'sinatra'
require 'fast_jsonapi'
require 'xapian'
require_relative 'lib/site'
require_relative 'lib/query'
require_relative 'lib/result'

# Mostrar la portada con la política de privacidad y acceso al código
# fuente.
get '/' do
  haml :index
end

# Hacer la búsqueda en sitio e idioma
get '/:site/:lang' do
  site = params[:site]
  lang = params[:lang]
  q    = params[:q]
  full = params[:f].present?
  site = Site.new site: site, lang: lang

  halt 403, 'WTF' && return unless site.indexed? || !q.empty?

  query  = Query.new path: site.db_path,
    query: q,
    lang: site.lang,
    full: full

  result = Result.new q
  result.results = query.search

  # El resultado
  content_type 'application/vnd.api+json'
  ResultSerializer.new(result).serialized_json
end
