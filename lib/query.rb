# Copyright (C) 2018 f <f@kefir.red>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.
#
# Abre una base de datos y realiza una búsqueda, devolviendo los
# resultados
class Query
  attr_reader :path, :query, :lang, :full

  LANG_MAP = {
    'en' => 'english',
    'es' => 'spanish',
    'ar' => 'arabic',
    'pt' => 'portuguese'
  }.freeze

  def initialize(path:, query:, lang:, full: false)
    @lang  = LANG_MAP[lang]
    @path  = path
    @query = query
    @full  = false

    open_database
    self
  end

  def open_database
    # Elementos necesarios para hacer la búsqueda
    @db = Xapian::Database.new @path
    @eq = Xapian::Enquire.new @db
    @qp = Xapian::QueryParser.new

    # Propiedades necesarias para entender la consulta
    @qp.stemmer  = Xapian::Stem.new @lang
    @qp.database = @db
    @qp.stemming_strategy = Xapian::QueryParser::STEM_SOME

    # Procesar la consulta
    @eq.query = @qp.parse_query @query
  end

  def search
    @eq.mset(0, 1000).matches.map do |m|
      j = JSON.load(m.document.data)
      j.delete 'content' unless full?

      j
    end
  end

  def full?
    @full
  end
end
