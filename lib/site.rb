# Copyright (C) 2018 f <f@kefir.red>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.
#
# Representa un sitio con su base de datos
class Site
  attr_reader :site, :lang

  def initialize(site:, lang: '')
    @site = site
    @lang = lang
  end

  def path
    File.join @site, @lang
  end

  def exist?
    Dir.exist? path
  end

  def db_path
    File.join '_sites', path, '.xapian'
  end

  def indexed?
    Dir.exist? db_path
  end
end
