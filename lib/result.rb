# Copyright (C) 2018 f <f@kefir.red>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.
#
# Los resultados de la base de datos
class Result
  attr_reader :query
  attr_accessor :results

  def initialize(query)
    @query = query
    @results = []
  end

  # TODO tiene alguna utilidad generar un ID relacionado a la búsqueda?
  def id
    1
  end

  def count
    @results.size
  end
end

# Serializador de los resultados
class ResultSerializer
  include FastJsonapi::ObjectSerializer

  attributes :query, :results, :count
end
